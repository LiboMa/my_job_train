"""
### Tutorial Documentation
Documentation that goes along with the Airflow tutorial located
[here](https://airflow.apache.org/tutorial.html)
"""
# [START tutorial]
from datetime import timedelta

# [START import_module]
# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG
# Operators; we need this to operate!
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago

# [END import_module]

# [START default_args]
# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}
# [END default_args]

# [START instantiate_dag]
dag = DAG(
    'my_train_job',
    default_args=default_args,
    description='A tutorial DAG',
    #schedule_interval=timedelta(days=1),
    schedule_interval='*/3 * * * *',
)
# [END instantiate_dag]

# t1, t2 and t3 are examples of tasks created by instantiating operators
# [START basic_task]
t1 = BashOperator(
    task_id='print_date',
    bash_command='date >>/tmp/date_updates',
    dag=dag,
)

t2 = BashOperator(
    task_id='pull_code_from_repo',
    depends_on_past=False,
    bash_command='echo pull code from report >>/tmp/date_updates',
    #retries=3,
    dag=dag,
)

t3 = BashOperator(
    task_id='run_job_training_job',
    depends_on_past=False,
    bash_command='echo starting training>>/tmp/date_updates',
    #retries=3,
    dag=dag,
)

t4 = BashOperator(
    task_id='gatering_result_of_training_job',
    #depends_on_past=False,
    #bash_command='sleep 5',
    bash_command='echo gatering results>>/tmp/date_updates',
    retries=3,
    dag=dag,
)
# [END basic_task]

# [START documentation]
dag.doc_md = __doc__

t1.doc_md = """\
#### Task Documentation
You can document your task using the attributes `doc_md` (markdown),
`doc` (plain text), `doc_rst`, `doc_json`, `doc_yaml` which gets
rendered in the UI's Task Instance Details page.
![img](http://montcs.bloomu.edu/~bobmon/Semesters/2012-01/491/import%20soul.png)
"""
# [END documentation]

# [START jinja_template]
#templated_command = """
#{% for i in range(5) %}
#    echo "{{ ds }}"
#    echo "{{ macros.ds_add(ds, 7)}}"
#    echo "{{ params.my_param }}"
#{% endfor %}
#"""
#
#t3 = BashOperator(
#    task_id='templated',
#    depends_on_past=False,
#    bash_command=templated_command,
#    params={'my_param': 'Parameter I passed in'},
#    dag=dag,
#)
# [END jinja_template]

t1 >> [t2, t3, t4]
# [END tutorial]
